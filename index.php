<!DOCTYPE html>
<html>
<head>
	<title>Hello world</title>
</head>
<body>
<h1>Hello world! -using HTML</h1>
<?php
	echo "<h2> Hello world! -using PHP </h2>"; 
?>
<h3 id="helloWorld"></h3>

<script type="text/javascript">
	var helloWorldElement = document.getElementById('helloWorld');
	var text = "Hello world! - using JavaScript";
	helloWorldElement.innerHTML = text;
</script>
</body>
</html>